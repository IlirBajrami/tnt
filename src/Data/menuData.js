export const menuData = [
  {
    title: "About",
    link: "/",
  },
  {
    title: "Services",
    link: "/services",
  },
  {
    title: "Portfolio",
    link: "/portfolio",
  },
  {
    title: "Team",
    link: "/team",
  },
  {
    title: "Contact",
    link: "/contact",
  },
];

export const HamburgerMenuData = [
  {
    title: "About",
    link: "/",
  },
  {
    title: "Services",
    link: "/services",
  },
  {
    title: "Portfolio",
    link: "/portfolio",
  },
  {
    title: "Team",
    link: "/team",
  },
  {
    title: "Contact",
    link: "/contact",
  },
];
