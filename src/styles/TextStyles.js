import styled from "styled-components";

export const BigTitle = styled.h1`
  font-weight: 800;
  font-size: 65px;

  @media (max-width: 450px) {
    font-size: 34px;
  }
`;

export const H2 = styled.h2`
  font-weight: bold;
  font-size: 40px;

  @media (max-width: 450px) {
    font-size: 28px;
  }
`;

export const GreetingText = styled.h3`
  font-weight: 800;
  font-size: 20px;

  @media (max-width: 450px) {
    font-size: 16px;
  }
`;

export const Smalltitle = styled.p`
  font-weight: 700;
  font-size: 24px;
  line-height: 140%;
`;

export const SmallText = styled.p`
  font-weight: 400;
  font-size: 16px;
  line-height: 130%;
  color: #5e5e5e;
`;
