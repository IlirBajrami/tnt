import "./App.css";
import Slider from "./components/Layout/Slider";
import MainPage from "./MainPage";
import "./Reset.css";

function App() {
  return <MainPage />;
}

export default App;
