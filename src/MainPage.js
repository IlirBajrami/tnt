import React from "react";
import Header from "./components/Layout/Header";
import styled from "styled-components";
import HeroSection from "./components/Layout/HeroSection";

function MainPage(props) {
  return (
    <MainWrapper>
      <Header />
      <BordersWrapper>
        <HeroSection />
      </BordersWrapper>
    </MainWrapper>
  );
}

export default MainPage;

const MainWrapper = styled.div`
  width: 100%;

  //background-color: red;
`;
const BordersWrapper = styled.div`
  box-sizing: border-box;
  position: relative;
  //width: 100%;
  height: auto;
  margin: 75px 0 0 30px;

  :before {
    content: "";
    //box-sizing: border-box;
    position: absolute;
    left: 0;
    bottom: 0;
    height: 95%;
    width: 2px;
    border-left: 2px solid #cdcdcd;
  }
  :after {
    content: "";
    box-sizing: border-box;
    position: absolute;
    right: 0;
    top: 0;
    width: 55%;
    height: 2px;
    border-top: 2px solid #cdcdcd;
    z-index: -1;
    //box-shadow: inset 2px 2px 2px 2px #cdcdcd;
  }
`;
