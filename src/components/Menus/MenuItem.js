import React from "react";
import styled from "styled-components";

function MenuItem(props) {
  const { item } = props;
  return (
    <div>
      <MenuLink title={item.title} href={item.link}>
        {item.title}
      </MenuLink>
    </div>
  );
}

export default MenuItem;

const MenuLink = styled.a`
  color: #939393;
  display: grid;

  align-items: center;
  padding: 10px;
  transition: 0.5s ease-out;
  font-weight: 600;
  font-size: 18px;

  :hover,
  &.active {
    background: rgba(255, 255, 255, 0.1);
    box-shadow: 0px 10px 20px rgba(0, 0, 0, 0.1),
      inset 0px 0px 0px 0.5px rgba(255, 255, 255, 0.2);
  }
`;
