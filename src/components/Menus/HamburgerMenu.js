import React, { useState, useRef } from "react";
import styled from "styled-components";

import { menuData } from "../../Data/menuData";
import MenuItem from "./MenuItem";

function HamburgerMenu(props) {
  const { hamburgerOpen } = props;

  return (
    <Wrapper hamburgerOpen={hamburgerOpen}>
      {menuData.map((item, index) => (
        <MenuItem item={item} key={index} />
      ))}
    </Wrapper>
  );
}

export default HamburgerMenu;

const Wrapper = styled.div`
  background: white;
  box-shadow: 0px 50px 100px rgba(0, 0, 0, 0.25),
    inset 0px 0px 0px 0.5px rgba(255, 255, 255, 0.2);

  padding: 20px;
  position: absolute;
  top: 60px;
  right: 30px;
  opacity: ${(props) => (props.hamburgerOpen ? 1 : 0)};
  z-index: 1;
  display: grid;
  gap: 10px;
  grid-template-columns: 150px;
  transition: 0.3s ease-in-out;
  //display: ${(props) => (props.isOpen ? "block" : "none")};
  visibility: ${(props) => (props.hamburgerOpen ? "visible" : "hidden")};
  transform: ${(props) =>
    props.hamburgerOpen
      ? "skewY(0deg) rotate(0deg) translateY(0px)"
      : "skewY(-5deg) rotate(5deg) translateY(-30px)"};
`;
