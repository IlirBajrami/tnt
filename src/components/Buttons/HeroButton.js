import React from "react";
import styled from "styled-components";

function HeroButton(props) {
  const { item } = props;
  return (
    <Wrapper href={item.link} onClick={props.onClick}>
      <TextWrapper>
        <Title>{item.title}</Title>
      </TextWrapper>
      <IconWrapper>
        <Icon src={item.icon} alt={item.title} className="icon" />
      </IconWrapper>
    </Wrapper>
  );
}

export default HeroButton;

const Wrapper = styled.a`
  width: 300px;
  height: 75px;
  padding: 12px 20px;
  background: #ff9700;

  display: grid;
  grid-template-columns: auto 53px;
  align-items: center;
  gap: 10px;

  *,
  & {
    transition: 1s cubic-bezier(0.075, 0.82, 0.165, 1);
  }

  :hover {
    box-shadow: 0px 1px 3px rgb(0 0 0 / 10%), 0px 10px 30px rgb(0 0 0 / 30%),
      inset 0px 0px 0px 0.5px rgb(255 255 255 / 42%);
    transform: translateY(-3px);

    .icon {
      transform: scale(1.2);
    }
  }

  @media (max-width: 768px) {
    transform: scale(0.9) !important;
    transform-origin: top left;
  }
  @media (max-width: 450px) {
    transform: scale(0.7) !important;
    transform-origin: top left;
  }
`;
const TextWrapper = styled.div`
  display: grid;
  gap: 4px;
`;

const Title = styled.p`
  color: white;
  font-weight: 600;
  font-size: 22px;
`;

const Icon = styled.img`
  width: 29px;
  height: 29px;
`;

const IconWrapper = styled.div`
  width: 45px;
  height: 45px;

  display: grid;
  justify-content: center;
  align-content: center;
  justify-self: center;
  position: relative;
`;
