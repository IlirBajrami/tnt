import React from "react";

import styled from "styled-components";

function HamburgerButton(props) {
  const { item } = props;
  return (
    <MenuItem onClick={item.onClick}>
      <img src={item.icon} alt={item.title} />
    </MenuItem>
  );
}

const MenuItem = styled.a`
  cursor: pointer;
  color: rgba(255, 255, 255, 0.7);
  display: grid;
  grid-template-columns: 24px auto;
  gap: ${(props) => (props.title ? "10px" : "0px")};
  align-items: center;
  padding: 10px;
  transition: 0.5s ease-out;

  background: rgba(255, 255, 255, 0.1);
  box-shadow: 0px 10px 20px rgba(0, 0, 0, 0.1),
    inset 0px 0px 0px 0.5px rgba(255, 255, 255, 0.2);
`;

export default HamburgerButton;
