import React, { useState, useRef, useEffect } from "react";
import styled from "styled-components";

import { images } from "../../Data/sliderData";
import { Smalltitle, SmallText } from "../../styles/TextStyles";

const delay = 4000;

function Slider(props) {
  //const picturesArray = [1, 2, 3, 4];

  const [index, setIndex] = useState(0);
  const timeoutRef = useRef(null);

  function resetTimeout() {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }
  }

  useEffect(() => {
    resetTimeout();
    timeoutRef.current = setTimeout(
      () =>
        setIndex((prevIndex) =>
          prevIndex === images.length - 1 ? 0 : prevIndex + 1
        ),
      delay
    );

    return () => {
      resetTimeout();
    };
  }, [index]);

  return (
    <Slideshow>
      <SlideshowSlider
        style={{ transform: `translate3d(${-index * 100}%, 0, 0)` }}
        //style={{ opacity: `${index ? 1 : 0}` }}
      >
        {images.map((Image, x) => (
          <Slide
            key={x}
            style={{
              backgroundImage: `url(${Image.src})`,
              //opacity: `${index === x ? 1 : 0}`,
            }}
          >
            <Captions
              style={{
                opacity: `${index === x ? 1 : 0}`,
              }}
            >
              <Subtitle>{Image.subtitle}</Subtitle>
              <Title>{Image.title}</Title>
            </Captions>
          </Slide>
        ))}
      </SlideshowSlider>

      <SlideIndicators>
        {images.map((_, idx) => (
          <div
            key={idx}
            className={`slideIndicator${index === idx ? " active" : ""}`}
            onClick={() => {
              setIndex(idx);
            }}
          ></div>
        ))}
      </SlideIndicators>
    </Slideshow>
  );
}

export default Slider;

const Slideshow = styled.div`
  margin: 0 auto;
  overflow: hidden;

  //max-width: 100%;
`;

const SlideshowSlider = styled.div`
  white-space: nowrap;
  transition: ease 500ms;
`;

const Slide = styled.div`
  position: relative;
  display: inline-block;

  min-height: 100vh;
  width: 100%;

  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
`;

const Captions = styled.div`
  position: absolute;
  left: 0;
  bottom: 0px;
  padding: 30px;
  text-align: left;
  transition: ease 4000ms;
`;

const Subtitle = styled(SmallText)`
  color: #a4a4a4;
`;

const Title = styled(Smalltitle)`
  color: white;
`;

const SlideIndicators = styled.div`
  position: absolute;
  right: 0;
  bottom: 0px;
  padding: 30px;
  text-align: right;
  .slideIndicator {
    display: inline-block;
    height: 4px;
    width: 100px;

    cursor: pointer;
    margin: 15px 7px 0px;

    background-color: white;
  }
  .active {
    background-color: #ff9700;
  }
`;
