import React from "react";
import styled, { keyframes } from "styled-components";
import HeroButton from "../Buttons/HeroButton";
import { BigTitle, GreetingText } from "../../styles/TextStyles";
import HeroBox from "./HeroBox";
import Slider from "./Slider";

import ButtonArrow from "../../resources/icons/Arrow_right.svg";
import WebDesignIcon from "../../resources/icons/Web-design-icon.svg";
import ArrowRed from "../../resources/icons/Arrow-right-red.svg";
import PlanningIcon from "../../resources/icons/Planning-icon.svg";
import ArrowBlue from "../../resources/icons/Arrow-right-blue.svg";
import WebIcon from "../../resources/icons/Web-icon.svg";
import ArrowGreen from "../../resources/icons/Arrow-right-green.svg";

function HeroSection(props) {
  return (
    <Wrapper>
      <ContentWrapper>
        <TextWrapper>
          <Description>Hello World!</Description>
          <Title>
            We are creative agency. <br />
            We build digital work that you will love!
          </Title>
          <Bigbutton>
            <HeroButton
              item={{
                icon: ButtonArrow,
                title: "Explore our portfolio",
                link: "#",
              }}
            />
          </Bigbutton>
          <BoxesWrapper>
            <HeroBox
              icon={WebDesignIcon}
              title="Web Design"
              description="Sed ut perspiciatis unde omni iste natus error sit volunteer accusantum doloremque."
              arrow={ArrowRed}
              height="330px"
              borderColor="#DF0D25"
            />
            <HeroBox
              icon={PlanningIcon}
              title="Web Design"
              description="Sed ut perspiciatis unde omni iste natus error sit volunte."
              arrow={ArrowBlue}
              height="255px"
              borderColor="#0071D8"
            />
            <HeroBox
              icon={WebIcon}
              title="Web Design"
              description="Sed ut perspiciatis unde omni iste natus error sit volunteer accusantum doloremque. Ut perspiciatis unde omni iste natus error sit volunteer accusantum doloremque."
              arrow={ArrowGreen}
              height="420px"
              borderColor="#00D879"
            />
          </BoxesWrapper>
        </TextWrapper>

        <SliderWrapper>
          <Slider />
        </SliderWrapper>
      </ContentWrapper>
    </Wrapper>
  );
}

export default HeroSection;

const animation = keyframes`
0% { opacity: 0; transform: translateY(-10px); filter: blur(10px); }
100% { opacity: 1; transform: translateY(0px); filter: blur(0px); }
`;

const Wrapper = styled.div`
  overflow: hidden;
`;

const ContentWrapper = styled.div`
  //max-width: 1440px;
  margin: 0 auto;
  padding: 200px 80px 0;
  display: grid;
  grid-template-columns: repeat(2, 1fr);

  @media (max-width: 450px) {
    grid-template-columns: auto;
    gap: 60px;
    padding: 150px 20px 250px;
  }
`;

const TextWrapper = styled.div`
  //max-width: 560px;
  display: grid;
  gap: 10px;

  > * {
    opacity: 0;
    animation: ${animation} 2s forwards;

    :nth-child(1) {
      animation-delay: 0s;
    }
    :nth-child(2) {
      animation-delay: 0.2s;
    }
    :nth-child(3) {
      animation-delay: 0.4s;
    }
  }
`;

const Description = styled(GreetingText)`
  color: #9e9e9e;
  font-weight: 800;
  text-transform: uppercase;
  font-size: 20px;
`;

const Title = styled(BigTitle)`
  color: black;
  font-weight: 800;
  font-size: 65px;
  @media (max-width: 450px) {
    font-size: 48px;
  }
`;

const Bigbutton = styled.div`
  margin: 50px 0;
`;

const BoxesWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: 5px;
  align-items: end;
  justify-content: center;
  //padding: 0 30px;
  > * {
    opacity: 0;
    animation: ${animation} 2s forwards;

    :nth-child(1) {
      animation-delay: 0.5s;
    }
    :nth-child(2) {
      animation-delay: 0.7s;
    }
    :nth-child(3) {
      animation-delay: 0.9s;
    }
  }
`;

const SliderWrapper = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  width: 45%;
  height: 100vh;
  /* background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url("/resources/img/slider_image2.jpeg"); */
`;
