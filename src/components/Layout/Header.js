import React, { useRef, useState } from "react";
import styled from "styled-components";
import { menuData } from "../../Data/menuData";
import MenuItem from "../Menus/MenuItem";
import HamburgerMenu from "../Menus/HamburgerMenu";
import logo from "../../resources/TNT_Logo.png";
import HamburgerButton from "../Buttons/HamburgerButton";

function Header(props) {
  const [hamburgerOpen, setHamburgerOpen] = useState();

  const ref = useRef();
  const hamburgerRef = useRef();

  function handleHamburgerClick(event) {
    setHamburgerOpen(!hamburgerOpen);
    event.preventDefault();
  }
  return (
    <>
      <Wrapper>
        <Logo>
          <img src={logo} alt="Logo" />
        </Logo>

        <MenuWrapper count={menuData.length} ref={ref}>
          {menuData.map((item, index) => {
            return <MenuItem item={item} key={index} />;
          })}
        </MenuWrapper>

        <HamburgerWrapper
          onClick={(event) => {
            handleHamburgerClick(event);
          }}
          ref={hamburgerRef}
        >
          <HamburgerButton
            item={{
              icon: "/resources/icons/hamburger.svg",
            }}
          />
          <HamburgerMenu hamburgerOpen={hamburgerOpen} />
        </HamburgerWrapper>
      </Wrapper>
    </>
  );
}

export default Header;

const Wrapper = styled.div`
  position: absolute;
  top: 42px;
  display: grid;
  grid-template-columns: 64px auto;
  width: 100%;
  padding: 0 30px;
  align-items: center;
  justify-content: start;

  @media (max-width: 768px) {
    top: 30px;
  }
  @media (max-width: 450px) {
    top: 20px;
    padding: 0 20px;
  }
`;

const MenuWrapper = styled.div`
  display: grid;
  gap: 40px;
  grid-template-columns: repeat(${(props) => props.count}, auto);
  padding-left: 30px;

  @media (max-width: 768px) {
    > a,
    div {
      display: none;
    }
    grid-template-columns: auto;
    //border-bottom: 1px solid red;
  }
`;

const Logo = styled.a`
  img {
    max-width: 64px;
    //padding-right: 40px;
  }
`;

const HamburgerWrapper = styled.div`
  display: none;

  @media (max-width: 768px) {
    display: block;
    position: absolute;
    top: 0;
    right: 35px;
    z-index: 1;
  }
`;
