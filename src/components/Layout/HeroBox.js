import React from "react";
import styled from "styled-components";
import { Smalltitle, SmallText } from "../../styles/TextStyles";

function HeroBox(props) {
  return (
    <BoxWrapper height={props.height} borderColor={props.borderColor}>
      <ContentWrapper>
        <Icon src={props.icon} alt={props.title} />
        <Title>{props.title}</Title>
        <Description>{props.description}</Description>
        <Arrow src={props.arrow} />
      </ContentWrapper>
    </BoxWrapper>
  );
}

export default HeroBox;

const BoxWrapper = styled.div`
  position: relative;
  width: 250px;
  height: ${(props) => props.height};
  border-top: 4px solid ${(props) => props.borderColor};
  background-color: #fbfbfb;
  padding: 45px 25px 25px;
`;

const ContentWrapper = styled.div`
  display: grid;
  justify-content: space-between;
  grid-template-rows: repeat(4, auto);
`;

const Icon = styled.img`
  padding-bottom: 20px;
`;

const Title = styled(Smalltitle)``;

const Description = styled(SmallText)``;

const Arrow = styled.img`
  position: absolute;
  bottom: 20px;
  right: 20px;
`;
